from migen import *
from migen.genlib.fsm import *

"""
UART_Tx module

Author: NIIBE Yutaka <gniibe@fsij.org>

This file is a part of Im_GPL, a hardware module for users' libre computing.

Im_GPL is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For detail, please read accompanied documentation.
"""

class UART_Tx(Module):
    def __init__(self,enable_parity=False,parity_odd=False):
        self.strobe = Signal()
        self.data_in = Signal(8)
        self.serial_out = Signal()

        data = Signal(8)
        if enable_parity:
            self.parity = Signal()

        fsm = FSM(reset_state="IDLE")
        self.submodules += fsm

        fsm.act("IDLE",
            NextValue(self.serial_out, 1),
            If(self.strobe,
                NextValue(data, self.data_in),
                NextState("START")
            )
        )
        if enable_parity:
            initial_par = 1 if parity_odd else 0
            fsm.act("START",
                NextValue(self.serial_out, 0),
                NextValue(self.parity, initial_par),
                NextState("DATA0")
            )
        else:
            fsm.act("START",
                NextValue(self.serial_out, 0),
                NextState("DATA0")
            )
        for i in range(8):
            state = "DATA%d" % i
            if enable_parity:
                next_state = "PARITY" if i == 7 else "DATA%d" % (i+1)
            else:
                next_state = "STOP" if i == 7 else "DATA%d" % (i+1)
            if enable_parity:
                fsm.act(state,
                    NextValue(self.serial_out, data[0]),
                    NextValue(data, Cat(data[1:8], 0)),
                    NextValue(self.parity, self.parity ^ data[0]),
                    NextState(next_state)
                )
            else:
                fsm.act(state,
                    NextValue(self.serial_out, data[0]),
                    NextValue(data, Cat(data[1:8], 0)),
                    NextState(next_state)
                )
        if enable_parity:
            fsm.act("PARITY",
                         NextValue(self.serial_out, self.parity),
                         NextState("STOP")
            )
        fsm.act("STOP",
            NextValue(self.serial_out, 1),
            NextState("IDLE")
        )

DEFAULT_CHAR_CYCLES=12
class UART_Tx_test(Module):
    def __init__(self,msg,char_cycles=DEFAULT_CHAR_CYCLES):
        self.strobe_out = Signal()
        self.data_out = Signal(8)

        len_msg=len(msg)
        counter_bits = Signal(max=char_cycles)
        counter_chars= Signal(max=len_msg)

        self.sync += \
            If (counter_bits == char_cycles - 1,
                self.strobe_out.eq(1),
                counter_bits.eq(0),
            ).Else(
                counter_bits.eq(counter_bits+1)
            )

        self.sync += \
            If (counter_bits == DEFAULT_CHAR_CYCLES//2 - 1,
                self.strobe_out.eq(0)
            )

        self.sync += \
            If (((counter_bits == char_cycles - 1) & (counter_chars >= len_msg - 1)),
                counter_chars.eq(0)
            )

        self.sync += \
            If (((counter_bits == char_cycles - 1) & (counter_chars < len_msg - 1)),
                counter_chars.eq(counter_chars + 1)
            )

        cases = {}
        for i in range(len_msg):
            cases[i] = self.data_out.eq(char2sig(msg[i]))

        self.sync += \
            If (counter_bits == char_cycles//2 - 1,
                Case(counter_chars, cases)
            )

def test_UART_Tx(m,msg,char_cycles=DEFAULT_CHAR_CYCLES):
    yield                       # one cycle for state machine
    # Cycles with no serial output
    for cycle in range(char_cycles):
        yield
    #
    counter = 0
    char = 0
    charbits = char2bits(msg[0])
    for cycle in range(2*len(msg)*char_cycles):
        yield
        counter = counter + 1
        if counter >= char_cycles:
            counter = 0
            char += 1
            if char >= len(msg):
                char = 0
            charbits = char2bits(msg[char])
        cyc = cycle % char_cycles
        if cyc == 0:
            assert (yield m.serial_out)==0
        elif cyc >= 1 and cyc <= 8:
            assert (yield m.serial_out)==charbits[cyc - 1]
        elif cyc == 9:      # Parity bit: even parity
            parity_bit = reduce(xor, charbits)
            assert (yield m.serial_out)==parity_bit
            print(charbits, parity_bit, " : ", msg[char] if msg[char].isprintable() else ord(msg[char]))
        else:
            assert (yield m.serial_out)==1

if __name__ == "__main__":
    import sys
    from operator import xor
    from serial_bits import *
    test_message = "GNU's Not Unix!\r\n"

    if len(sys.argv) >= 2 and sys.argv[1] == "sim":
        if len(sys.argv) > 2:
            test_message = sys.argv[2]
        m = Module()
        u_gen = UART_Tx_test(test_message)
        u = UART_Tx(enable_parity=True)
        m.submodules += u_gen
        m.submodules += u
        m.serial_out = Signal()
        m.comb += u.strobe.eq(u_gen.strobe_out)
        m.comb += u.data_in.eq(u_gen.data_out)
        m.comb += m.serial_out.eq(u.serial_out)

        run_simulation(m, test_UART_Tx(m,test_message),
                       vcd_name="uart_tx.vcd")
    elif len(sys.argv) >= 2 and sys.argv[1] == "build":
        #
        # $ stty -F /dev/ttyUSB1 300 raw parenb
        # $ cat /dev/ttyUSB1 
        # GNU's Not Unix!
        #

        from prescaler import PreScaler
        from migen.fhdl.decorators import ClockDomainsRenamer

        # Now, we assume the HX8K board
        from migen.build.generic_platform import *
        from migen.build.platforms import ice40_hx8k_b_evn

        io_ext = [ ("serial_tx", 2, Pins("B12"),  IOStandard("LVCMOS33")) ]

        plat = ice40_hx8k_b_evn.Platform()
        plat.add_extension(io_ext)
        serial_out = plat.request("serial_tx")
        clk12M = plat.request("clk12")

        strobe = Signal()
        data = Signal(8)

        m = Module()
        ps = PreScaler(40000)   # 300Hz
        m.submodules += ps
        m.clock_domains.cd_sys = ClockDomain("sys", reset_less=True)
        m.comb += m.cd_sys.clk.eq(clk12M)

        u_gen = UART_Tx_test(test_message,64)
        u = UART_Tx(enable_parity=True)
        run_at_clk300 = ClockDomainsRenamer("clk300")
        m.submodules += run_at_clk300(u_gen)
        m.submodules += run_at_clk300(u)
        m.clock_domains.cd_clk300 = ClockDomain("clk300",reset_less=True)
        m.comb += m.cd_clk300.clk.eq(ps.clk_out)

        m.comb += u.strobe.eq(u_gen.strobe_out)
        m.comb += u.data_in.eq(u_gen.data_out)
        m.comb += serial_out.eq(u.serial_out)

        plat.build(m,use_nextpnr=False,build_name="uart_tx")
        if len(sys.argv) >= 3 and sys.argv[2] == "-l":
            prog = plat.create_programmer()
            prog.load_bitstream("build/uart_tx.bin")
