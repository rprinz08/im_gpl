"""
MsgGen module

Author: NIIBE Yutaka <gniibe@fsij.org>

This file is a part of Im_GPL, a hardware module for users' libre computing.

Im_GPL is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For detail, please read accompanied documentation.
"""

from migen import *
from uart_tx import UART_Tx
from serial_bits import char2sig

MSG="I'm under GPL.\r\n"
#
# Space part
#           Message part
#
#           I'm under GPL.\r\n
# <-Spaces-><--Fixed Message-->
#   0 .. 15    0 .. 15
#
class MsgGen(Module):
    def __init__(self,slow,enable_parity=False,parity_odd=False):
        self.serial_out = Signal()

        self.strobe = Signal()
        self.char_generated = Signal(8)
        counter = Signal(13)
        if slow > 0:
            counter_turn = Signal(max=slow*512)
        cycle_for_char_output    = counter[0:4]
        cycle_for_char_in_string = counter[4:8]
        cycle_for_msg_or_spaces  = counter[8:9]
        cycle_for_num_of_spaces  = counter[9:13]
        cases = {}
        for i in range(len(MSG)):
            cases[i] = \
                If( cycle_for_msg_or_spaces == 1,
                    self.char_generated.eq(char2sig(MSG[i]))
                ).Else(
                    self.char_generated.eq(char2sig(' '))
                )
        if slow > 0:
            self.comb += self.strobe.eq((counter_turn == counter[4:13]*slow) \
                                        & (cycle_for_char_output == 8) \
                                        & ((cycle_for_msg_or_spaces == 1)
                                           | ((cycle_for_char_in_string) > 15 - cycle_for_num_of_spaces)))
        else:
            self.comb += self.strobe.eq((cycle_for_char_output == 8) \
                                         & ((cycle_for_msg_or_spaces == 1)
                                            | ((cycle_for_char_in_string) > 15 - cycle_for_num_of_spaces)))

        self.sync += counter.eq(counter + 1)
        self.sync += Case(cycle_for_char_in_string, cases)
        if slow > 0:
            self.sync += \
                If(counter==4095,
                   If( counter_turn==slow*512-1,
                       counter_turn.eq(0)
                   ).Else(
                       counter_turn.eq(counter_turn + 1)
                   )
                )
        uart = UART_Tx(enable_parity,parity_odd)
        self.submodules += uart
        self.comb += uart.strobe.eq(self.strobe)
        self.comb += uart.data_in.eq(self.char_generated)
        self.comb += self.serial_out.eq(uart.serial_out)
