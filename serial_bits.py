"""
serial_bits.py - Character to bits or list of Signal

Author: NIIBE Yutaka <gniibe@fsij.org>

This file is a part of Im_GPL, a hardware module for users' libre computing.

Im_GPL is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For detail, please read accompanied documentation.
"""

from migen import *

def char2bits(ch):
    return [((ord(ch) >> i) & 1) for i in range(8)]

def char2sig(ch):
    l = char2bits(ch)
    return Cat(l)
