# Im_GPL: a hardware module for users' libre computing

_If your hardware could whisper, what would it say?_

Im_GPL is a hardware module that can be embedded in any target to notify the
user that his/her/theirs computing freedom is respected. The way it works is by 
dedicating one test pin to expose a 16-character string with the free license 
using a standard +3.3v TTL signal, communicated via serial protocol with
parameters that can be configured at build time:

`"I'm under GPL.\r\n"`

## Targets

The project is being developed using an iCE40HX8K-based board. It is also tested
against iCE40UP5K targets, and many development boards are known to work just 
fine, such as:

- Olimex's iCE40HX-EVB (recommended for being 'Open Hardware'), with:

  - OLIMEXINO-32U4 as programmer
  - IDC10-15cm cable
  - USB mini cable
  - Possibly, SY0605E 5V power supply

- iCE40-HX8K Breakout Board
- TinyFPGA BX (with iCE40LP8K)
- iCE40 UltraPlus Breakout Board

## Dependencies 

This project uses ['migen'](https://m-labs.hk/migen/manual/introduction.html#installing-migen) for generating the HDL code.
Im_GPL and 'migen' depend on Python 3.5 or superior version.

For FPGA synthesis, we use 'Project IceStorm' tools:

- [IceStorm tools](http://www.clifford.at/icestorm/)
- [Arachne-PNR](https://github.com/YosysHQ/arachne-pnr)
- [Yosys](http://www.clifford.at/yosys/)

On Debian GNU/Linux systems, you can install the following packages:

- 'fpga-icestorm'
- 'arachne-pnr'
- 'yosys'

## How to build

After having all the dependencies installed, you can generate the necessary
files for FPGA synthesis using the following command:

``python3 im_gpl.py build``

Alternatively, you can run a simulation to be visualized with 'gtkwave'
or 'sigrok' (which outputs a vcd file: 'uart-tx.vcd'):

``python3 im_gpl.py sim``

## License

This project is licensed under GPLv3. See LICENSE for further details.

