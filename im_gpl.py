"""
Im_GPL module

Author: NIIBE Yutaka <gniibe@fsij.org>

This file is a part of Im_GPL, a hardware module for users' libre computing.

Im_GPL is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For detail, please read accompanied documentation.
"""

from migen import *
from msg_gen import MsgGen, MSG
from serial_bits import char2bits
from prescaler import PreScaler

from functools import reduce
from operator import xor

class Im_GPL(Module):
    def __init__(self,prescaler_modulus=104, slow=1, enable_parity=False):
        self.serial_out = Signal()
        if prescaler_modulus == 1:
            msg_gen = MsgGen(slow, enable_parity)
            self.submodules.msg_gen = msg_gen
            self.comb += self.serial_out.eq(msg_gen.serial_out)
        else:
            prescaler = PreScaler(prescaler_modulus)
            self.submodules.prescaler = prescaler
            run_at_clkUART = ClockDomainsRenamer("clkUART")
            self.clock_domains.cd_clkUART = ClockDomain("clkUART", reset_less=True)
            self.comb += self.cd_clkUART.clk.eq(prescaler.clk_out)

            msg_gen = MsgGen(slow, enable_parity)
            self.submodules.msg_gen = run_at_clkUART(msg_gen)
            self.comb += self.serial_out.eq(msg_gen.serial_out)

REPEAT_COUNT=2
def test_module(m):
    # one clock delay for state machine
    yield
    # 7 clock dead cycles because strobe at 8 (after char generation)
    yield
    yield
    yield
    yield
    yield
    yield
    yield
    for i in range(len(MSG)*2*16*REPEAT_COUNT):
        it_is_spaces = (((i >> 4)&1) == 0)
        char_in_string = ((i) & 15)
        num_of_spaces = ((i >> 5) & 15)
        if it_is_spaces:
            if char_in_string > 15 - num_of_spaces:
                strobe = 1
            else:
                strobe = 0
        else:
            strobe = 1
        assert (yield m.msg_gen.strobe) == strobe

        data=(yield m.msg_gen.char_generated)
        if strobe:
            assert data == ord(' ' if it_is_spaces else MSG[i%len(MSG)])
        serial_bits = char2bits(chr(data))
        serial_bits.append(reduce(xor, serial_bits))

        if strobe:
            print(chr(data), end='')
        for j in range(16):
            yield
            serial=(yield m.serial_out)
            if strobe:
                if (j == 0):
                    assert (serial == 1)
                elif (j == 1):
                    assert (serial == 0)
                elif (j >= 11):
                    assert (serial == 1)
                else:
                    assert (serial == serial_bits[j-2])
            else:
                assert (serial == 1)

if __name__ == "__main__":
    import sys
    if len(sys.argv) == 2 and sys.argv[1] == "sim":
        m = Im_GPL(1,0,True)
        run_simulation(m, test_module(m), vcd_name="im_gpl.vcd")
    elif len(sys.argv) >= 2 and sys.argv[1] == "build":
        #
        # $ stty -F /dev/ttyUSB1 115200 raw -parenb
        # $ cat /dev/ttyUSB1 
        # ...
        #            I'm under GPL.
        #             I'm under GPL.
        #
        from migen.build.generic_platform import *
        from migen.build.platforms import ice40_hx8k_b_evn

        io_ext = [ ("serial_tx", 2, Pins("B12"),  IOStandard("LVCMOS33")) ]

        plat = ice40_hx8k_b_evn.Platform()
        plat.add_extension(io_ext)
        serial_out = plat.request("serial_tx")
        clk12 = plat.request("clk12")

        clk_pll_out = Signal()

        # PLL output 72MHz, 115200bps, slow=3
        m = Im_GPL(625, 3)
        m.comb += serial_out.eq(m.serial_out)
        m.clock_domains.cd_sys = ClockDomain("sys", reset_less=True)
        m.comb += m.cd_sys.clk.eq(clk_pll_out)

        m.specials.pll = Instance("SB_PLL40_CORE",
                                  p_FEEDBACK_PATH = "PHASE_AND_DELAY",
                                  p_DIVR = 0,
                                  p_DIVF = 5,
                                  p_DIVQ = 3,
                                  p_FILTER_RANGE=1,
                                  p_PLLOUT_SELECT = "SHIFTREG_0deg",
                                  p_ENABLE_ICEGATE = 0,
                                  i_REFERENCECLK = clk12,
                                  i_BYPASS = 0,
                                  i_RESETB = 1,
                                  o_PLLOUTCORE = clk_pll_out)
        plat.build(m,use_nextpnr=False,build_name="im_gpl")
        if len(sys.argv) >= 3 and sys.argv[2] == "-l":
            prog = plat.create_programmer()
            prog.load_bitstream("build/im_gpl.bin")
    else:
        from migen.fhdl import verilog

        serial_out = Signal()
        m = Im_GPL()
        print(verilog.convert(m, {m.serial_out}, name="im_gpl"))
